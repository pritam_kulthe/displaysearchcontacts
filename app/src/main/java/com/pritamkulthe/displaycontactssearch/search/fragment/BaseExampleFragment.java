package com.pritamkulthe.displaycontactssearch.search.fragment;

import android.content.Context;
import androidx.fragment.app.Fragment;

import com.arlib.floatingsearchview.FloatingSearchView;

public abstract class BaseExampleFragment extends Fragment {

    private BaseExampleFragmentCallbacks callbacks;

    public interface BaseExampleFragmentCallbacks{
        void onAttachSearchViewToDrawer(FloatingSearchView searchView);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof BaseExampleFragmentCallbacks) {
            callbacks = (BaseExampleFragmentCallbacks) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement BaseExampleFragmentCallbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    protected void attachSearchViewActivityDrawer(FloatingSearchView searchView){
        if(callbacks != null){
            callbacks.onAttachSearchViewToDrawer(searchView);
        }
    }

    public abstract boolean onActivityBackPress();
}
