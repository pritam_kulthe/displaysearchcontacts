package com.pritamkulthe.displaycontactssearch.search.fragment;

import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.pritamkulthe.displaycontactssearch.R;
import com.pritamkulthe.displaycontactssearch.search.adapter.SearchResultsListAdapter;
import com.pritamkulthe.displaycontactssearch.search.data.ContactsSuggestion;
import com.pritamkulthe.displaycontactssearch.search.data.DataHelper;
import com.pritamkulthe.displaycontactssearch.utils.Contact;
import com.arlib.floatingsearchview.FloatingSearchView;
import com.arlib.floatingsearchview.suggestions.SearchSuggestionsAdapter;
import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;
import com.arlib.floatingsearchview.util.Util;

import java.util.List;

public class SlidingSearchResultsExampleFragment extends BaseExampleFragment {

    private final String TAG = "BlankFragment";
    public static final long FIND_SUGGESTION_SIMULATED_DELAY = 250;
    private FloatingSearchView searchView;
    private RecyclerView searchResultsList;
    private SearchResultsListAdapter searchResultsAdapter;
    private boolean isDarkSearchTheme = false;
    private String lastQuery = "";

    public SlidingSearchResultsExampleFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sliding_search_results_example, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        searchView = (FloatingSearchView) view.findViewById(R.id.floating_search_view);
        searchResultsList = (RecyclerView) view.findViewById(R.id.search_results_list);

        setupFloatingSearch();
        setupContactsResultsList();
        setupDrawer();
    }

    private void setupFloatingSearch() {
        searchView.setOnQueryChangeListener(new FloatingSearchView.OnQueryChangeListener() {

            @Override
            public void onSearchTextChanged(String oldQuery, final String newQuery) {

                if (!oldQuery.equals("") && newQuery.equals("")) {
                    searchView.clearSuggestions();
                } else {

                    searchView.showProgress();

                    DataHelper.findSuggestions(getActivity(), newQuery, 5,
                            FIND_SUGGESTION_SIMULATED_DELAY, new DataHelper.OnFindSuggestionsListener() {

                                @Override
                                public void onResults(List<ContactsSuggestion> results) {

                                    searchView.swapSuggestions(results);
                                    searchView.hideProgress();
                                }
                            });
                }
                Log.d(TAG, "onSearchTextChanged()");
            }
        });

        searchView.setOnSearchListener(new FloatingSearchView.OnSearchListener() {
            @Override
            public void onSuggestionClicked(final SearchSuggestion searchSuggestion) {

                ContactsSuggestion contactSuggestion = (ContactsSuggestion) searchSuggestion;
                DataHelper.findContacts(getActivity(), contactSuggestion.getBody(),
                        new DataHelper.OnFindContactsListener() {

                            @Override
                            public void onResults(List<Contact> results) {
                                searchResultsAdapter.swapData(results);
                            }

                        });
                Log.d(TAG, "onSuggestionClicked()");

                lastQuery = searchSuggestion.getBody();
            }

            @Override
            public void onSearchAction(String query) {
                lastQuery = query;

                DataHelper.findContacts(getActivity(), query,
                        new DataHelper.OnFindContactsListener() {

                            @Override
                            public void onResults(List<Contact> results) {
                                searchResultsAdapter.swapData(results);
                            }

                        });
                Log.d(TAG, "onSearchAction()");
            }
        });

        searchView.setOnFocusChangeListener(new FloatingSearchView.OnFocusChangeListener() {
            @Override
            public void onFocus() {

                searchView.swapSuggestions(DataHelper.getHistory(getActivity(), 3));

                Log.d(TAG, "onFocus()");
            }

            @Override
            public void onFocusCleared() {

                searchView.setSearchBarTitle(lastQuery);
                Log.d(TAG, "onFocusCleared()");
            }
        });

        searchView.setOnMenuItemClickListener(new FloatingSearchView.OnMenuItemClickListener() {
            @Override
            public void onActionMenuItemSelected(MenuItem item) {

                if (item.getItemId() == R.id.action_change_colors) {

                    isDarkSearchTheme = true;

                    searchView.setBackgroundColor(Color.parseColor("#787878"));
                    searchView.setViewTextColor(Color.parseColor("#e9e9e9"));
                    searchView.setHintTextColor(Color.parseColor("#e9e9e9"));
                    searchView.setActionMenuOverflowColor(Color.parseColor("#e9e9e9"));
                    searchView.setMenuItemIconColor(Color.parseColor("#e9e9e9"));
                    searchView.setLeftActionIconColor(Color.parseColor("#e9e9e9"));
                    searchView.setClearBtnColor(Color.parseColor("#e9e9e9"));
                    searchView.setDividerColor(Color.parseColor("#BEBEBE"));
                    searchView.setLeftActionIconColor(Color.parseColor("#e9e9e9"));
                } else {

                    //just print
                    Toast.makeText(getActivity().getApplicationContext(), item.getTitle(),
                            Toast.LENGTH_SHORT).show();
                }

            }
        });

        searchView.setOnHomeActionClickListener(new FloatingSearchView.OnHomeActionClickListener() {
            @Override
            public void onHomeClicked() {
                Log.d(TAG, "onHomeClicked()");
            }
        });

        searchView.setOnBindSuggestionCallback(new SearchSuggestionsAdapter.OnBindSuggestionCallback() {
            @Override
            public void onBindSuggestion(View suggestionView, ImageView leftIcon,
                                         TextView textView, SearchSuggestion item, int itemPosition) {
                ContactsSuggestion contactSuggestion = (ContactsSuggestion) item;

                String textColor = isDarkSearchTheme ? "#ffffff" : "#000000";
                String textLight = isDarkSearchTheme ? "#bfbfbf" : "#787878";

                if (contactSuggestion.getIsHistory()) {
                    leftIcon.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                            R.drawable.ic_history_black_24dp, null));

                    Util.setIconColor(leftIcon, Color.parseColor(textColor));
                    leftIcon.setAlpha(.36f);
                } else {
                    leftIcon.setAlpha(0.0f);
                    leftIcon.setImageDrawable(null);
                }

                textView.setTextColor(Color.parseColor(textColor));
                String text = contactSuggestion.getBody()
                        .replaceFirst(searchView.getQuery(),
                                "<font color=\"" + textLight + "\">" + searchView.getQuery() + "</font>");
                textView.setText(Html.fromHtml(text));
            }

        });

        searchView.setOnSuggestionsListHeightChanged(new FloatingSearchView.OnSuggestionsListHeightChanged() {
            @Override
            public void onSuggestionsListHeightChanged(float newHeight) {
                searchResultsList.setTranslationY(newHeight);
            }
        });
    }

    private void setupContactsResultsList() {
        searchResultsAdapter = new SearchResultsListAdapter(getActivity());
        searchResultsList.setAdapter(searchResultsAdapter);
        searchResultsList.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public boolean onActivityBackPress() {
        if (!searchView.setSearchFocused(false)) {
            return false;
        }
        return true;
    }

    private void setupDrawer() {
        attachSearchViewActivityDrawer(searchView);
    }

}
