package com.pritamkulthe.displaycontactssearch.search.adapter;
import android.app.Activity;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;
import android.widget.Toast;

import com.pritamkulthe.displaycontactssearch.R;
import com.pritamkulthe.displaycontactssearch.utils.Contact;
import com.arlib.floatingsearchview.util.Util;

import java.util.ArrayList;
import java.util.List;

public class SearchResultsListAdapter extends RecyclerView.Adapter<SearchResultsListAdapter.ViewHolder> {

    private Context context;
    public SearchResultsListAdapter(Context context) {
        this.context = context;
    }
    public SearchResultsListAdapter() {
    }

    private List<Contact> dataSet = new ArrayList<>();
    private int lastAnimatedItemPosition = -1;

    public interface OnItemClickListener{
        void onClick(Contact colorWrapper);
    }

    private OnItemClickListener itemsOnClickListener;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView contactName;
        public final TextView contactNumber;
        public final TextView email;
        public final View mView;

        public ViewHolder(View view) {
            super(view);
            contactName = (TextView) view.findViewById(R.id.tvName);
            contactNumber = (TextView) view.findViewById(R.id.tvPhone);
            email = (TextView) view.findViewById(R.id.tvEmail);
            mView = view;
        }
    }

    public void swapData(List<Contact> mNewDataSet) {
        dataSet = mNewDataSet;
        notifyDataSetChanged();
    }

    public void setItemsOnClickListener(OnItemClickListener onClickListener){
        this.itemsOnClickListener = onClickListener;
    }

    @Override
    public SearchResultsListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_contact_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchResultsListAdapter.ViewHolder holder, final int position) {

        final Contact contactSuggestion = dataSet.get(position);
        holder.contactName.setText(contactSuggestion.getName());

        String totalNumbers = "";
        // to join the all numbers
        for (int i = 0; i < contactSuggestion.getNumbers().size(); ++i)
        {
            totalNumbers += contactSuggestion.getNumbers().get(i).number;

            if ((i != contactSuggestion.getNumbers().size()-1) && (contactSuggestion.getNumbers().size() > 1))
                totalNumbers += ", ";
        }

        String totalEmails = "";
        // to join all emails
        for (int i = 0; i < contactSuggestion.getEmails().size(); ++i)
        {
            totalEmails += contactSuggestion.getEmails().get(i).address;

            if ((i != contactSuggestion.getEmails().size()-1) && (contactSuggestion.getEmails().size() > 1))
                totalEmails += ", ";
        }

        holder.contactNumber.setText(totalNumbers);
        holder.email.setText(totalEmails);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(context, ""+contactSuggestion.getNumbers().get(0).number, Toast.LENGTH_SHORT).show();
            }
        });


        if(lastAnimatedItemPosition < position){
            animateItem(holder.itemView);
            lastAnimatedItemPosition = position;
        }

        if(itemsOnClickListener != null){
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemsOnClickListener.onClick(dataSet.get(position));
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    private void animateItem(View view) {
        view.setTranslationY(Util.getScreenHeight((Activity) view.getContext()));
        view.animate()
                .translationY(0)
                .setInterpolator(new DecelerateInterpolator(3.f))
                .setDuration(700)
                .start();
    }
}

