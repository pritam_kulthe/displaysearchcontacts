package com.pritamkulthe.displaycontactssearch.search.data;

import android.os.Parcel;

import com.arlib.floatingsearchview.suggestions.model.SearchSuggestion;

public class ContactsSuggestion implements SearchSuggestion {

    private String contactName;
    private boolean isHistory = false;

    public ContactsSuggestion(String suggestion) {
        this.contactName= suggestion.toLowerCase();
    }

    public ContactsSuggestion(Parcel source) {
        this.contactName= source.readString();
        this.isHistory = source.readInt() != 0;
    }

    public void setIsHistory(boolean isHistory) {
        this.isHistory = isHistory;
    }

    public boolean getIsHistory() {
        return this.isHistory;
    }

    @Override
    public String getBody() {
        return contactName;
    }

    public static final Creator<ContactsSuggestion> CREATOR = new Creator<ContactsSuggestion>() {
        @Override
        public ContactsSuggestion createFromParcel(Parcel in) {
            return new ContactsSuggestion(in);
        }

        @Override
        public ContactsSuggestion[] newArray(int size) {
            return new ContactsSuggestion[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(contactName);
        dest.writeInt(isHistory ? 1 : 0);
    }
}

