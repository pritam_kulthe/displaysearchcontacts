package com.pritamkulthe.displaycontactssearch.search;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AppCompatActivity;

import com.pritamkulthe.displaycontactssearch.R;
import com.pritamkulthe.displaycontactssearch.search.fragment.BaseExampleFragment;
import com.pritamkulthe.displaycontactssearch.search.fragment.SlidingSearchResultsExampleFragment;
import com.arlib.floatingsearchview.FloatingSearchView;

import java.util.List;

public class SearchActivity extends AppCompatActivity implements BaseExampleFragment.BaseExampleFragmentCallbacks {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        showFragment(new SlidingSearchResultsExampleFragment());
    }

    @Override
    public void onBackPressed() {
        List fragments = getSupportFragmentManager().getFragments();
        BaseExampleFragment currentFragment = (BaseExampleFragment) fragments.get(fragments.size() - 1);

        if (!currentFragment.onActivityBackPress()) {
            super.onBackPressed();
        }
    }

    private void showFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment).commit();
    }

    @Override
    public void onAttachSearchViewToDrawer(FloatingSearchView searchView) {
        //searchView.attachNavigationDrawerToMenuButton(mLeftDrawerLayout);
    }
}
