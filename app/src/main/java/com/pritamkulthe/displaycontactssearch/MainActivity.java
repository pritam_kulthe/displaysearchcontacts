package com.pritamkulthe.displaycontactssearch;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.pritamkulthe.displaycontactssearch.search.SearchActivity;
import com.pritamkulthe.displaycontactssearch.search.util.Constants;
import com.pritamkulthe.displaycontactssearch.utils.Contact;
import com.pritamkulthe.displaycontactssearch.utils.ContactFetcher;
import com.pritamkulthe.displaycontactssearch.utils.ContactsRecyclerAdapter;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView contactsRV;
    ArrayList<Contact> contactsList;
    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        }
        else
        {
            displayContactsList();
        }
    }

    private void displayContactsList()
    {
        contactsRV = (RecyclerView) findViewById(R.id.contactsRecyclerView);
        contactsList = new ContactFetcher(this).fetchAll();

        Constants.CONTACTLIST = contactsList;

        ContactsRecyclerAdapter recyclerAdapter = new ContactsRecyclerAdapter(contactsList, MainActivity.this, R.layout.adapter_contact_item);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        contactsRV.setLayoutManager(llm);
        contactsRV.setAdapter(recyclerAdapter);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // contacts-related task you need to do.
                    displayContactsList();

                } else {
                    // functionality that depends on this permission or ask for permission again.
                    ActivityCompat.requestPermissions(MainActivity.this,
                            new String[]{Manifest.permission.READ_CONTACTS},
                            MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                }
                return;
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.menu_search)
        {
            startActivity(new Intent(MainActivity.this, SearchActivity.class));
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_corner_menu, menu);
        return true;
    }
}