package com.pritamkulthe.displaycontactssearch.utils;

/**
 * Created by aupadhyay on 5/8/17.
 */

public class ContactEmail {
    public String address;
    public String type;

    public ContactEmail(String address, String type) {
        this.address = address;
        this.type = type;
    }
}
